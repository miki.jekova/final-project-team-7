package com.teamseven.addonis;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**This is a class that runs Spring Boot based unit tests.
 */

@SpringBootTest
class AddonisApplicationTests {

	@Test
	void contextLoads() {
	}
}
