package com.teamseven.addonis.services;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.IDE;
import com.teamseven.addonis.repositories.contracts.IDEsRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;

/**
 * This is a class that contains unit tests and thus tests the proper functioning of methods
 * in the IDesServiceImpl Class
 */

@RunWith(MockitoJUnitRunner.class)
public class IDesServiceImplTests {

    @Mock
    IDEsRepository idEsRepository;

    @InjectMocks
    IDEsServiceImpl mockService;

    @Test
    public void findAll_Should_CallRepository() {
        //Act
        mockService.getAll();

        Mockito.verify(idEsRepository,
                times(1)).findAll();
    }

    @Test
    public void getIdeByID_Should_CallRepository() throws NotFoundException {

        IDE ide = new IDE();
        ide.setIDEId(2);

        Mockito.when(idEsRepository.existsByIDEId(2))
                .thenReturn(true);
        //Act
        mockService.getIdeByID(2);

        Mockito.verify(idEsRepository,
                times(1)).findIDEByIDEId(2);
    }

    @Test
    public void findIDEByIDEName_Should_CallRepository() throws NotFoundException {

        IDE ide = new IDE();
        ide.setIDEName("NAME");

        Mockito.when(idEsRepository.existsByIDEName("NAME"))
                .thenReturn(true);
        //Act
        mockService.getIdeByName("NAME");

        Mockito.verify(idEsRepository,
                times(1)).findIDEByIDEName("NAME");
    }

    @Test
    public void createIDE_Should_CallRepository() throws DuplicateException {

        IDE ide = new IDE();
        ide.setIDEName("NAME");

        Mockito.when(idEsRepository.existsByIDEName("NAME"))
                .thenReturn(false);
        //Act
        mockService.createIDE(ide);

        Mockito.verify(idEsRepository,
                times(1)).saveAndFlush(ide);
    }

    @Test
    public void getIdeById_Should_ThrowNotFoundException() {

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getIdeByID(1);
        });
    }

    @Test
    public void getIdeByName_Should_ThrowNotFoundException() {

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getIdeByName("name");
        });
    }

    @Test
    public void createIDE_Should_ThrowDuplicateException() {

        IDE ide = new IDE();
        ide.setIDEName("NAME");

        Mockito.when(idEsRepository.existsByIDEName("NAME"))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateException.class, () -> {
            mockService.createIDE(ide);
        });
    }

}
