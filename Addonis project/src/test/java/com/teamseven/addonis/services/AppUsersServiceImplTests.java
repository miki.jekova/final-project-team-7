package com.teamseven.addonis.services;

import com.teamseven.addonis.repositories.contracts.AppUserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;

/**
 * This is a class that contains unit tests and thus tests the proper functioning of methods
 * in the AppUsersServiceImpl Class.
 */

@RunWith(MockitoJUnitRunner.class)
public class AppUsersServiceImplTests {

    @Mock
    AppUserRepository appUserRepository;

    @InjectMocks
    AppUsersServiceImpl mockService;

    @Test
    public void getById_Should_CallRepository() {
        //Act
        mockService.getById(1);

        Mockito.verify(appUserRepository,
                times(1)).findById(1);
    }
}
