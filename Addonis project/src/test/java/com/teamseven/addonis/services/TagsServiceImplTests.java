package com.teamseven.addonis.services;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.Tag;
import com.teamseven.addonis.repositories.contracts.TagsRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;

/**
 * This is a class that contains unit tests and thus tests the proper functioning of methods
 * in the TagsServiceImpl Class
 */

@RunWith(MockitoJUnitRunner.class)
public class TagsServiceImplTests {

    @Mock
    TagsRepository tagsRepository;

    @InjectMocks
    TagsServicesImpl mockService;

    @Test
    public void findAll_Should_CallRepository() {
        //Act
        mockService.getAll();

        Mockito.verify(tagsRepository,
                times(1)).findAll();
    }

    @Test
    public void findTagById_Should_CallRepository() throws NotFoundException {

        Tag tag = new Tag();
        tag.setTagId(2);

        Mockito.when(tagsRepository.existsByTagId(2))
                .thenReturn(true);

        //Act
        mockService.getTagById(2);

        Mockito.verify(tagsRepository,
                times(1)).findTagByTagId(2);
    }

    @Test
    public void createTag_Should_CallRepository() throws DuplicateException {

        Tag tag = new Tag();
        tag.setTag("TAG");

        Mockito.when(tagsRepository.existsByTag("TAG"))
                .thenReturn(false);
        //Act
        mockService.createTag(tag);

        Mockito.verify(tagsRepository,
                times(1)).saveAndFlush(tag);
    }

    @Test
    public void findTagByTag_Should_CallRepository() throws NotFoundException {

        Tag tag = new Tag();
        tag.setTag("TAG");

        Mockito.when(tagsRepository.existsByTag("TAG"))
                .thenReturn(true);

        //Act
        mockService.getTagByName("TAG");

        Mockito.verify(tagsRepository,
                times(1)).findTagByTag("TAG");
    }

    @Test
    public void getTagById_Should_ThrowNotFoundException() {

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getTagById(1);
        });
    }

    @Test
    public void getTagByName_Should_ThrowNotFoundException() {

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getTagByName("name");
        });
    }

    @Test
    public void createTag_Should_createTag() {

        Tag tag = new Tag();
        tag.setTag("da");

        Mockito.when(tagsRepository.existsByTag("da"))
                .thenReturn(true);
        Assertions.assertThrows(DuplicateException.class, () -> {
            mockService.createTag(tag);
        });
    }

}
