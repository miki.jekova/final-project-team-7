package com.teamseven.addonis.services;

import com.teamseven.addonis.models.Addon;
import com.teamseven.addonis.models.Rating;
import com.teamseven.addonis.models.User;
import com.teamseven.addonis.repositories.contracts.RatingsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;

/**
 * This is a class that contains unit tests and thus tests the proper functioning of methods
 * in the RatingsServiceImpl Class
 */

@RunWith(MockitoJUnitRunner.class)
public class RatingsServiceImplTests {

    @Mock
    RatingsRepository ratingsRepository;

    @InjectMocks
    RatingsServiceImpl mockService;

    @Test
    public void getAddonsRating_Should_CallRepository() {

        Addon addon = new Addon();
        addon.setAddonId(1);
        //Act
        mockService.getAddonsRating(addon);

        Mockito.verify(ratingsRepository,
                times(1)).findAverageRatingOfAddon(1);
    }

    @Test
    public void getAddonsRatingById_Should_CallRepository() {

        //Act
        mockService.getAddonsRatingById(1);

        Mockito.verify(ratingsRepository,
                times(1)).findByRatingId(1);
    }

    @Test
    public void rateAddon_Should_SaveNewRating() {

        Addon addon = new Addon();
        addon.setAddonId(1);

        User user = new User();
        user.setId(1);

        Rating rating = new Rating();
        rating.setAddon(addon);
        rating.setUser(user);

        Mockito.when(ratingsRepository.existsByUser_IdAndAddon_AddonId(1, 1))
                .thenReturn(false);
        //Act
        mockService.rateAddon(rating);

        Mockito.verify(ratingsRepository,
                times(1)).save(rating);
    }

    @Test
    public void rateAddon_Should_SaveExistingRating() {

        Addon addon = new Addon();
        addon.setAddonId(1);
        User user = new User();
        user.setId(1);

        Rating newRating = new Rating();
        newRating.setUser(user);
        newRating.setAddon(addon);
        newRating.setRatingVal(4);

        Rating ratingOld = new Rating();
        ratingOld.setRatingVal(55);
        ratingOld.setAddon(addon);
        ratingOld.setUser(user);

        Mockito.when(ratingsRepository.existsByUser_IdAndAddon_AddonId(1, 1))
                .thenReturn(true);

        Mockito.when(ratingsRepository.findByUser_IdAndAddon_AddonId(1, 1)).
                thenReturn(ratingOld);

        ratingOld.setRatingVal(newRating.getRatingVal());

        //Act
        mockService.rateAddon(newRating);

        Mockito.verify(ratingsRepository,
                times(1)).save(ratingOld);
    }

}
