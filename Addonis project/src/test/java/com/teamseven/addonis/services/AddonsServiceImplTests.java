package com.teamseven.addonis.services;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotAuthorisedException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.*;
import com.teamseven.addonis.repositories.contracts.AddonsRepository;
import com.teamseven.addonis.repositories.contracts.BinaryFilesRepository;
import com.teamseven.addonis.repositories.contracts.IDEsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;

/**
 * This is a class that contains unit tests and thus tests the proper functioning of methods
 * in the AddonsServiceImpl Class
 */

@RunWith(MockitoJUnitRunner.class)
public class AddonsServiceImplTests {

    @Mock
    AddonsRepository addonsRepository;

    @Mock
    BinaryFilesRepository binaryFilesRepository;

    @Mock
    IDEsRepository idEsRepository;

    @InjectMocks
    AddonsServiceImpl mockService;

    @Test
    public void findAll_Should_CallRepository() {

        //Act
        mockService.findAll();

        Mockito.verify(addonsRepository,
                times(1)).findAllByEnabledIsTrueAndStatus_Status("approved");
    }

    @Test
    public void getAddonById_Should_ReturnAddon_WhenAddonExists() {

        Addon addon = new Addon();
        addon.setAddonId(1);

        Mockito.when(addonsRepository.findByAddonId(1))
                .thenReturn(addon);

        //Act
        Addon returnedAddon = addonsRepository.findByAddonId(1);

        Assert.assertEquals(returnedAddon.getAddonId(), addon.getAddonId());
    }

    @Test
    public void getAddonById_Should_ThrowNotFoundExceptionWhenAddonAlreadyExist() {

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getAddonById(1);
        });

    }

    @Test
    public void getAllAddonsOrderByName_Should_CallRepository() {

        //Act
        mockService.getAllAddonsOrderByName();

        Mockito.verify(addonsRepository,
                times(1)).findAllByEnabledIsTrueAndStatus_StatusOrderByNameAsc("approved");
    }

    @Test
    public void getAllAddonsOrderByDate_Should_CallRepository() {

        //Act
        mockService.getAllAddonsOrderByDate();

        Mockito.verify(addonsRepository,
                times(1)).findAllByEnabledIsTrueAndStatus_StatusOrderByUploadDateDesc("approved");
    }

    @Test
    public void getFourPopularAddons_Should_CallRepository() {

        //Act
        mockService.getEightPopularAddons();

        Mockito.verify(addonsRepository,
                times(1)).findTop8ByEnabledIsTrueAndStatus_StatusOrderByDownloadsDesc("approved");
    }

    @Test
    public void getFourFutureAddons_Should_CallRepository() {

        //Act
        mockService.getFourFeaturedAddons();

        Mockito.verify(addonsRepository,
                times(1)).findTop4ByEnabledIsTrueAndStatus_StatusOrderByAddonId("approved");
    }

    @Test
    public void getFourNewestAddons_Should_CallRepository() {

        //Act
        mockService.getSixNewestAddons();

        Mockito.verify(addonsRepository,
                times(1)).findTop6ByEnabledIsTrueAndStatus_StatusOrderByUploadDateDesc("approved");
    }

    @Test
    public void getAddonByNumberOfDownloads_Should_CallRepository() {

        //Act
        mockService.getAddonsByNumberOfDownloads();

        Mockito.verify(addonsRepository,
                times(1)).findAllByEnabledIsTrueAndStatus_StatusOrderByDownloadsDesc("approved");
    }

    @Test
    public void createAddon_Should_CallRepository() throws DuplicateException {

        Addon addon = new Addon();
        addon.setName("ADDON");

        Mockito.when(addonsRepository.existsAddonByNameAndEnabledIsTrue(addon.getName()))
                .thenReturn(false);
        //Act
        mockService.createAddon(addon);

        Mockito.verify(addonsRepository,
                times(1)).saveAndFlush(addon);
    }


    @Test
    public void getAddonByName_Should_CallRepository() throws NotFoundException {

        Addon addon = new Addon();
        addon.setName("ADDON");

        Mockito.when(addonsRepository.existsAddonByNameAndEnabledIsTrue(addon.getName()))
                .thenReturn(true);
        //Act
        mockService.getAddonByName("ADDON");

        Mockito.verify(addonsRepository,
                times(1)).findAddonByNameAndEnabledIsTrueAndStatus_Status("ADDON", "approved");
    }

    @Test
    public void getAddonById_Should_CallRepository() throws NotFoundException {

        Addon addon = new Addon();
        addon.setAddonId(1);

        Mockito.when(addonsRepository.existsAddonByAddonIdAndEnabledIsTrue(1))
                .thenReturn(true);

        //Act
        mockService.getAddonById(1);

        Mockito.verify(addonsRepository,
                times(1)).findByAddonId(1);
    }

    @Test
    public void getAddonByName_Should_ThrowNotFoundException() {

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getAddonByName("ADDON");
        });
    }

    @Test
    public void update_Should_ThrowNotFoundException() {

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.updateAddon(new Addon(), "dsdsd");
        });
    }

    @Test
    public void getAddonsByIde_Should_ThrowNotFoundException() {

        Addon addon = new Addon();
        IDE ide = new IDE();

        ide.setIDEName("BBB");
        addon.setIde(ide);

        Mockito.when(!idEsRepository.existsByIDEName("BBB"))
                .thenReturn(false);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getAddonsByIde("BBB");
        });
    }

    @Test
    public void getAddonsByCommitDate_Should_CallRepository() {

        //Act
        mockService.getAddonsByCommitDate();

        Mockito.verify(addonsRepository,
                times(1)).findAllByEnabledIsTrueAndStatus_StatusOrderByGitHubInfo_LastCommitDate("approved");
    }

    @Test
    public void createAddon_Should_DuplicateException() {

        Addon addon = new Addon();
        addon.setName("addon");

        Mockito.when(addonsRepository.existsAddonByNameAndEnabledIsTrue("addon"))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateException.class, () -> {
            mockService.createAddon(addon);
        });
    }

    @Test
    public void updateDownloads_Should_ThrowNotFoundException() {

        Addon addon = new Addon();
        addon.setAddonId(1);

        Mockito.when(!addonsRepository.existsAddonByAddonId(1))
                .thenReturn(false);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.updateState(addon);
        });
    }

    @Test
    public void updateDownloads_Should_CallRepository() throws NotFoundException {

        Addon addon = new Addon();
        addon.setAddonId(1);

        Mockito.when(!addonsRepository.existsAddonByAddonId(1))
                .thenReturn(true);

        mockService.updateState(addon);

        Mockito.verify(addonsRepository,
                times(1)).save(addon);
    }

    @Test
    public void getAddonsByIde_Should_CallRepository() throws NotFoundException {

        Addon addon = new Addon();
        addon.setAddonId(1);

        IDE ide = new IDE();
        ide.setIDEName("any");

        Mockito.when(!idEsRepository.existsByIDEName("any"))
                .thenReturn(true);

        mockService.getAddonsByIde("any");

        Mockito.verify(addonsRepository,
                times(1))
                .findAllByEnabledIsTrueAndIde_IDENameAndStatus_Status("any", "approved");
    }

    @Test
    public void deleteAddonById_Should_CallRepository() throws NotAuthorisedException {

        Addon addon = new Addon();
        addon.setAddonId(1);

        User user = new User();
        user.setUsername("username");

        addon.setCreator(user);

        Mockito.when(addonsRepository.findByAddonIdAndEnabledIsTrue(1))
                .thenReturn(addon);

        mockService.deleteAddonById(addon, "username");

        Mockito.verify(addonsRepository,
                times(1)).saveAndFlush(addon);
    }

    @Test
    public void createAddon_Should_CallBinaryFileRepository() throws DuplicateException {

        Addon addon = new Addon();
        addon.setName("Addon");

        Mockito.when(addonsRepository.existsAddonByNameAndEnabledIsTrue("Addon"))
                .thenReturn(false);

        BinaryFile binaryFile1 = new BinaryFile();
        addon.setBinaryFile(binaryFile1);

        //Act
        mockService.createAddon(addon);

        Mockito.verify(binaryFilesRepository,
                times(1)).save(addon.getBinaryFile());
    }

    @Test
    public void updateAddon_Should_CallBinaryFileRepository() throws NotFoundException, NotAuthorisedException {

        byte[] bytes = "Any String you want".getBytes();
        User user = new User();
        user.setUsername("user");
        Addon addon = new Addon();
        addon.setAddonId(1);

        Mockito.when(addonsRepository.existsAddonByAddonId(1))
                .thenReturn(true);

        BinaryFile binaryFile1 = new BinaryFile();
        binaryFile1.setBinaryFile(bytes);
        addon.setBinaryFile(binaryFile1);
        addon.setCreator(user);

        //Act
        mockService.updateAddon(addon, "user");

        Mockito.verify(binaryFilesRepository,
                times(1)).save(addon.getBinaryFile());
    }

    @Test
    public void updateAddon_Should_NotAuthorisedException() throws NotFoundException, NotAuthorisedException {

        User user = new User();
        user.setUsername("user");

        Addon addon = new Addon();
        addon.setAddonId(1);

        Mockito.when(addonsRepository.existsAddonByAddonId(1))
                .thenReturn(true);

        addon.setCreator(user);

        //Act
        mockService.updateAddon(addon, "user");

        Assertions.assertThrows(NotAuthorisedException.class, () -> {
            mockService.updateAddon(addon, "userMock");
        });
    }

    @Test
    public void findAllPendingAddons_Should_CallRepository() {

        //Act
        mockService.findAllPendingAddons();

        Mockito.verify(addonsRepository,
                times(1)).findAllByEnabledIsTrueAndStatus_Status("pending");
    }

    @Test
    public void getAllAddonsOfUSer_Should_CallRepository() {

        //Act
        mockService.getAllAddonsOfUser(1);

        Mockito.verify(addonsRepository,
                times(1)).findAllByEnabledIsTrueAndCreator_Id(1);
    }

    @Test
    public void approveAddon_Should_ThrowNotFoundException() {

        Addon addon = new Addon();
        addon.setAddonId(1);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.approveAddon(1);
        });
    }

    @Test
    public void approveAddon_Should_CallRepository() throws NotFoundException {

        Addon addon = new Addon();
        addon.setAddonId(1);

        Mockito.when(addonsRepository.existsAddonByAddonIdAndEnabledIsTrue(1))
                .thenReturn(true);

        Mockito.when(mockService.getAddonById(1)).
                thenReturn(addon);

        Status newStatus = new Status();
        newStatus.setStatus("approved");
        newStatus.setStatusId(2);
        addon.setStatus(newStatus);

        mockService.approveAddon(1);

        Mockito.verify(addonsRepository,
                times(1)).saveAndFlush(addon);
    }

}
