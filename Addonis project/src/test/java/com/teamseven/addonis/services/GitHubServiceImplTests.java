package com.teamseven.addonis.services;

import com.teamseven.addonis.models.GitHubInfo;
import com.teamseven.addonis.repositories.contracts.GitHubInfoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;

/**
 * This is a class that contains unit tests and thus tests the proper functioning of methods
 * in the GitHubServiceImpl Class.
*/

@RunWith(MockitoJUnitRunner.class)
public class GitHubServiceImplTests {

    @Mock
    GitHubInfoRepository gitHubInfoRepository;

    @InjectMocks
    GitHubServiceImpl mockService;

    @Test
    public void createGitHubInfo_Should_CallRepository() {

        GitHubInfo gitHubInfo = new GitHubInfo();
        //Act
        mockService.createGitHubInfo(gitHubInfo);

        Mockito.verify(gitHubInfoRepository,
                times(1)).saveAndFlush(gitHubInfo);
    }

}
