package com.teamseven.addonis.services;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.AppUser;
import com.teamseven.addonis.models.User;
import com.teamseven.addonis.repositories.contracts.AddonsRepository;
import com.teamseven.addonis.repositories.contracts.AppUserRepository;
import com.teamseven.addonis.repositories.contracts.UsersRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;

/**
 * This is a class that contains unit tests and thus tests the proper functioning of methods
 * in the UsersServiceImpl Class
 */

@RunWith(MockitoJUnitRunner.class)
public class UsersServiceImplTests {

    @Mock
    UsersRepository usersRepository;

    @Mock
    AppUserRepository appUserRepository;

    @Mock
    AddonsRepository addonsRepository;

    @InjectMocks
    UsersServiceImpl mockService;

    @Test
    public void getAll_Should_CallRepository() {

        mockService.getAll();

        Mockito.verify(usersRepository,
                times(1)).findAllByEnabledIsTrue();
    }

    @Test
    public void getByUsername_Should_ThrowNotFoundException() {

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getByUsername("username");
        });
    }

    @Test
    public void getByUsername_Should_CallRepository() throws NotFoundException {

        Mockito.when(usersRepository.existsByUsernameAndEnabledIsTrue("username"))
                .thenReturn(true);

        mockService.getByUsername("username");

        Mockito.verify(usersRepository,
                times(1)).findByUsernameAndEnabledIsTrue("username");
    }

    @Test
    public void getById_Should_CallRepository() throws NotFoundException {

        Mockito.when(usersRepository.existsByIdAndEnabledIsTrue(1))
                .thenReturn(true);

        mockService.getById(1);

        Mockito.verify(usersRepository,
                times(1)).findByIdAndEnabledIsTrue(1);
    }

    @Test
    public void getById_Should_getUserById() {

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getById(1);
        });
    }

    @Test
    public void createUser_Should_CallRepository() throws DuplicateException {

        User user = new User();
        user.setUsername("username");
        AppUser appUser = new AppUser();

        Mockito.when(usersRepository.existsByUsername("username"))
                .thenReturn(false);

        mockService.createUser(user, appUser);

        Mockito.verify(appUserRepository,
                times(1)).saveAndFlush(appUser);
    }

    @Test
    public void updateUser_Should_CallRepository() throws NotFoundException {

        User user = new User();
        user.setUsername("username");
        AppUser appUser = new AppUser();

        Mockito.when(usersRepository.existsByUsername("username"))
                .thenReturn(true);

        mockService.updateUser(user, appUser);

        Mockito.verify(appUserRepository,
                times(1)).saveAndFlush(appUser);
    }

    @Test
    public void createUser_Should_ThrowDuplicateException() {

        User user = new User();
        user.setUsername("username");
        AppUser appUser = new AppUser();

        Mockito.when(usersRepository.existsByUsername("username"))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateException.class, () -> {
            mockService.createUser(user, appUser);
        });
    }

    @Test
    public void updateUser_Should_ThrowNotFoundException() {

        User user = new User();
        user.setUsername("username");
        AppUser appUser = new AppUser();

        Mockito.when(usersRepository.existsByUsername("username"))
                .thenReturn(false);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.updateUser(user, appUser);
        });
    }

    @Test
    public void deleteUser_Should_ThrowNotFoundException() {

        User user = new User();
        user.setId(1);

        Mockito.when(usersRepository.existsById(1))
                .thenReturn(false);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.deleteUserById(1);
        });
    }

    @Test
    public void deleteUser_Should_CallRepository() throws NotFoundException {

        User user = new User();
        user.setId(1);

        Mockito.when(usersRepository.existsById(1))
                .thenReturn(true);

        Mockito.when(usersRepository.findByIdAndEnabledIsTrue(1))
                .thenReturn(user);

        mockService.deleteUserById(1);

        Mockito.verify(usersRepository,
                times(1)).saveAndFlush(user);
    }

    @Test
    public void getUserByUsername_Should_ThrowNotFound() {

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.deleteUserByUsername("");
        });
    }

    @Test
    public void deleteUserByUsername_Should_CallRepository() throws NotFoundException {

        User user = new User();
        user.setUsername("");
        user.setEnabled(true);

        Mockito.when(usersRepository.existsByUsername(""))
                .thenReturn(true);

        Mockito.when(usersRepository.findByUsernameAndEnabledIsTrue("")).
                thenReturn(user);

        mockService.deleteUserByUsername("");

        Mockito.verify(usersRepository,
                times(1)).saveAndFlush(user);
    }

}
