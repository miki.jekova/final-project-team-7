package com.teamseven.addonis.services;

import com.teamseven.addonis.repositories.contracts.StatusRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;

/**
 * This is a class that contains unit tests and thus tests the proper functioning of methods
 * in the StatusServiceImpl Class
 */

@RunWith(MockitoJUnitRunner.class)
public class StatusServiceImplTests {

    @Mock
    StatusRepository statusRepository;

    @InjectMocks
    StatusServiceImpl mockService;

    @Test
    public void getStatusByName_Should_CallRepository() {

        mockService.getStatusByName("name");

        Mockito.verify(statusRepository,
                times(1)).findByStatus("name");
    }
}
