package com.teamseven.addonis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class AddonisApplication {

	public static void main(String[] args) {
		SpringApplication.run(AddonisApplication.class, args);
	}

}
