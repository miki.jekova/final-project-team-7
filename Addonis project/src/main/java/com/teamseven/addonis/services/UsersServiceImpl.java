package com.teamseven.addonis.services;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.AppUser;
import com.teamseven.addonis.models.User;
import com.teamseven.addonis.repositories.contracts.AppUserRepository;
import com.teamseven.addonis.repositories.contracts.UsersRepository;
import com.teamseven.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * This is a class that provides implementation of the main functionality of the user.
 */
@Service
public class UsersServiceImpl implements UsersService {
    private UsersRepository usersRepository;
    private AppUserRepository appUserRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository,
                            AppUserRepository appUserRepository) {
        this.usersRepository = usersRepository;
        this.appUserRepository = appUserRepository;
    }

    @Override
    public List<User> getAll() {
        return usersRepository.findAllByEnabledIsTrue();
    }

    @Override
    public User getByUsername(String username) throws NotFoundException {
        if (!usersRepository.existsByUsernameAndEnabledIsTrue(username)) {
            throw new NotFoundException("User", "username", username);
        }
        return usersRepository.findByUsernameAndEnabledIsTrue(username);
    }

    @Override
    public User getById(int id) throws NotFoundException {
        if (!usersRepository.existsByIdAndEnabledIsTrue(id)) {
            throw new NotFoundException("User", "ID", String.valueOf(id));
        }
        return usersRepository.findByIdAndEnabledIsTrue(id);
    }

    @Override
    public void createUser(User user, AppUser appUser) throws DuplicateException {
        if (usersRepository.existsByUsername(user.getUsername())) {
            throw new DuplicateException("User", "username", user.getUsername());
        }
        appUserRepository.saveAndFlush(appUser);
        usersRepository.saveAndFlush(user);
    }

    @Override
    public void updateUser(User user, AppUser appUser) throws NotFoundException {
        if (!usersRepository.existsByUsername(user.getUsername())) {
            throw new NotFoundException("User", "username", user.getUsername());
        }
        appUserRepository.saveAndFlush(appUser);
        usersRepository.saveAndFlush(user);
    }

    @Override
    public void deleteUserById(int id) throws NotFoundException {
        if (!usersRepository.existsById(id)) {
            throw new NotFoundException("User", "ID", String.valueOf(id));
        }
        User deletedUser = usersRepository.findByIdAndEnabledIsTrue(id);
        deletedUser.setEnabled(false);
        usersRepository.saveAndFlush(deletedUser);
    }

    @Override
    public void deleteUserByUsername(String username) throws NotFoundException {
        if(!usersRepository.existsByUsername(username)) {
            throw new NotFoundException("User", "username", String.valueOf(username));
        }
        User userToDelete = usersRepository.findByUsernameAndEnabledIsTrue(username);

        Mapper.disableUserAddons(userToDelete.getId());

        userToDelete.setEnabled(false);
        usersRepository.saveAndFlush(userToDelete);
    }
}
