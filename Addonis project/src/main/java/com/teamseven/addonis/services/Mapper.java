package com.teamseven.addonis.services;

import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.*;
import com.teamseven.addonis.models.DTOs.AddonDTO;
import com.teamseven.addonis.models.DTOs.AddonUpdateDTO;
import com.teamseven.addonis.models.DTOs.RatingDTO;
import com.teamseven.addonis.models.DTOs.UserDTO;
import com.teamseven.addonis.repositories.contracts.AddonsRepository;
import com.teamseven.addonis.repositories.contracts.GitHubClientRepository;
import com.teamseven.addonis.services.contracts.*;
import com.teamseven.addonis.validators.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * This is a class that sets certain data on a particular object from a date transfer object.
 */

@Component
public class Mapper {

    private IDEsService idEsService;
    private UsersService usersService;
    private AddonsService addonsService;
    private GitHubClientRepository gitHubClientRepository;
    private GitHubService gitHubService;
    private static AddonsRepository addonsRepository;

    @Autowired
    public Mapper(IDEsService idEsService,
                  UsersService usersService,
                  AddonsService addonsService,
                  GitHubClientRepository gitHubClientRepository,
                  GitHubService gitHubService,
                  AddonsRepository addonsRepository) {
        this.idEsService = idEsService;
        this.usersService = usersService;
        this.addonsService = addonsService;
        this.gitHubClientRepository = gitHubClientRepository;
        this.gitHubService = gitHubService;
        Mapper.addonsRepository = addonsRepository;
    }

    public Addon toAddon(AddonDTO addonDTO, MultipartFile file) throws IOException, NotFoundException {

        Validator.checkUserInput(addonDTO,file);

        Addon addon = new Addon();
        addon.setName(addonDTO.getName());
        addon.setDescription(addonDTO.getDescription());
        addon.setOriginLink(addonDTO.getOriginLink());

        IDE ide = idEsService.getIdeByName(addonDTO.getIDE());
        addon.setIde(ide);

        BinaryFile addonBinary = new BinaryFile();
        addonBinary.setBinaryFile(file.getBytes());
        addon.setBinaryFile(addonBinary);

        GitHubInfo gitHubInfo = gitHubClientRepository.getRepositoryInfo(addonDTO.getOriginLink());
        gitHubService.createGitHubInfo(gitHubInfo);
        addon.setGitHubInfo(gitHubInfo);

        return addon;
    }

    public Addon mergeAddons(Addon oldAddon, AddonUpdateDTO updateDTO) throws NotFoundException {

        oldAddon.setName(getNotNull(updateDTO.getName(), oldAddon.getName()));
        oldAddon.setDescription(getNotNull(updateDTO.getDescription(), oldAddon.getDescription()));
        if (updateDTO.getIDE() != null) {
            oldAddon.setIde(idEsService.getIdeByName(updateDTO.getIDE()));
        }
        return oldAddon;
    }

    public Rating toRating(int addonId, RatingDTO ratingDTO, String username) throws NotFoundException {
        User user = usersService.getByUsername(username);
        Addon addon = addonsService.getAddonById(addonId);

        Rating rating = new Rating();
        rating.setRatingVal(ratingDTO.getRating());
        rating.setUser(user);
        rating.setAddon(addon);

        return rating;
    }

    public User mergeUsers(AppUser appUser, User oldUser, UserDTO userDTO) {

        appUser.setEmail(getNotNull(userDTO.getEmail(), appUser.getEmail()));
        appUser.setName(getNotNull(userDTO.getName(), appUser.getName()));
        return oldUser;
    }

    public <T> T getNotNull(T newValue, T oldValue) {
        return oldValue != null && newValue != null && !newValue.equals(oldValue) ? newValue : oldValue;
    }

    public static void disableUserAddons(int userId) {
        List<Addon> userAddons = addonsRepository.findAllByEnabledIsTrueAndCreator_Id(userId);

        for (Addon addon:userAddons) {
            addon.setEnabled(false);
            addonsRepository.saveAndFlush(addon);
        }
    }
}
