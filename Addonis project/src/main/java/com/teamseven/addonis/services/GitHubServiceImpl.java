package com.teamseven.addonis.services;

import com.teamseven.addonis.models.GitHubInfo;
import com.teamseven.addonis.repositories.contracts.GitHubInfoRepository;
import com.teamseven.addonis.services.contracts.GitHubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This is a class that creates GitHubInfo object
 * which provides information for the GitHub pull requests count, open issues count,
 * last commit date and title of the addon.
 */
@Service
public class GitHubServiceImpl implements GitHubService {

    private final GitHubInfoRepository gitHubInfoRepository;

    @Autowired
    public GitHubServiceImpl(GitHubInfoRepository gitHubInfoRepository) {
        this.gitHubInfoRepository = gitHubInfoRepository;
    }

    @Override
    public void createGitHubInfo(GitHubInfo gitHubInfo) {
        gitHubInfoRepository.saveAndFlush(gitHubInfo);
    }

}
