package com.teamseven.addonis.services;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.IDE;
import com.teamseven.addonis.repositories.contracts.IDEsRepository;
import com.teamseven.addonis.services.contracts.IDEsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This is a class that provides implementation of the main functionality of the IDE class.
 */
@Service
public class IDEsServiceImpl implements IDEsService {

    private final IDEsRepository idEsRepository;

    @Autowired
    public IDEsServiceImpl(IDEsRepository idEsRepository) {
        this.idEsRepository = idEsRepository;
    }

    @Override
    public List<IDE> getAll() {
        return idEsRepository.findAll();
    }

    @Override
    public IDE getIdeByID(int id) throws NotFoundException {
        if(!idEsRepository.existsByIDEId(id)){
            throw new NotFoundException("IDE", "id", String.valueOf(id));
        }
        return idEsRepository.findIDEByIDEId(id);
    }

    @Override
    public IDE getIdeByName(String name) throws NotFoundException {
        if(!idEsRepository.existsByIDEName(name)){
            throw new NotFoundException("IDE", "name", name);
        }
        return idEsRepository.findIDEByIDEName(name);
    }

    @Override
    public void createIDE(IDE ide) throws DuplicateException {
        if(idEsRepository.existsByIDEName(ide.getIDEName())){
            throw new DuplicateException("IDE", "name", ide.getIDEName());
        }
        idEsRepository.saveAndFlush(ide);
    }
}
