package com.teamseven.addonis.services;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.Tag;
import com.teamseven.addonis.repositories.contracts.TagsRepository;
import com.teamseven.addonis.services.contracts.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This is a class that provides implementation of the main functionality of the Tag class.
 */
@Service
public class TagsServicesImpl implements TagsService {

    private final TagsRepository tagsRepository;

    @Autowired
    public TagsServicesImpl(TagsRepository tagsRepository) {
        this.tagsRepository = tagsRepository;
    }

    @Override
    public List<Tag> getAll() {
        return tagsRepository.findAll();
    }

    @Override
    public Tag getTagById(int id) throws NotFoundException {
        if (!tagsRepository.existsByTagId(id)) {
            throw new NotFoundException("Tag", "id", String.valueOf(id));
        }
        return tagsRepository.findTagByTagId(id);
    }

    @Override
    public Tag getTagByName(String tag) throws NotFoundException {
        if (!tagsRepository.existsByTag(tag)) {
            throw new NotFoundException("Tag", "name", tag);
        }
        return tagsRepository.findTagByTag(tag);
    }

    @Override
    public void createTag(Tag tag) throws DuplicateException {
        if (tagsRepository.existsByTag(tag.getTag())) {
            throw new DuplicateException("Tag", "name", tag.getTag());
        }
        tagsRepository.saveAndFlush(tag);
    }
}
