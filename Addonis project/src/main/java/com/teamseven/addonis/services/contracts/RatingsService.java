package com.teamseven.addonis.services.contracts;

import com.teamseven.addonis.models.Addon;
import com.teamseven.addonis.models.Rating;

/**
 * This is a class that provides business functionality for the RatingsServiceImpl class.
 */

public interface RatingsService {

    void getAddonsRatingById(int id);

    Double getAddonsRating(Addon addon);

    void rateAddon(Rating rating);

}
