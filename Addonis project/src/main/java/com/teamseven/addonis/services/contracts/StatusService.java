package com.teamseven.addonis.services.contracts;


import com.teamseven.addonis.models.Status;
/**
 * This is a class that gets Status object from Status` name.
 */
public interface StatusService {
    Status getStatusByName(String name);
}
