package com.teamseven.addonis.services.contracts;

import com.teamseven.addonis.models.GitHubInfo;
/**
 * This is a class that provides functionality for creating the GitHub connection.
 */
public interface GitHubService {
    void createGitHubInfo(GitHubInfo gitHubInfo);
}
