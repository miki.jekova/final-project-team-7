package com.teamseven.addonis.services;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotAuthorisedException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.Addon;
import com.teamseven.addonis.models.Status;
import com.teamseven.addonis.repositories.contracts.AddonsRepository;
import com.teamseven.addonis.repositories.contracts.BinaryFilesRepository;
import com.teamseven.addonis.repositories.contracts.IDEsRepository;
import com.teamseven.addonis.services.contracts.AddonsService;
import com.teamseven.addonis.validators.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.teamseven.addonis.constants.Constants.*;

/**
 * This is a class that provides implementation of the main functionality of the Addon class.
 */
@Service
public class AddonsServiceImpl implements AddonsService {

    private AddonsRepository addonsRepository;
    private IDEsRepository idEsRepository;
    private BinaryFilesRepository binaryFilesRepository;

    @Autowired
    public AddonsServiceImpl(AddonsRepository addonsRepository, IDEsRepository idEsRepository,
                             BinaryFilesRepository binaryFilesRepository) {
        this.addonsRepository = addonsRepository;
        this.idEsRepository = idEsRepository;
        this.binaryFilesRepository = binaryFilesRepository;
    }

    @Override
    public List<Addon> findAll() {
        return addonsRepository
                .findAllByEnabledIsTrueAndStatus_Status(STATUS_APPROVED);
    }

    @Override
    public List<Addon> findAllPendingAddons() {
        return addonsRepository.findAllByEnabledIsTrueAndStatus_Status(STATUS_PENDING);
    }

    @Override
    public List<Addon> getAllAddonsOfUser(int userId) {
        return addonsRepository.findAllByEnabledIsTrueAndCreator_Id(userId);
    }

    @Override
    public Addon getAddonById(int id) throws NotFoundException {
        if (!addonsRepository.existsAddonByAddonIdAndEnabledIsTrue(id)) {
            throw new NotFoundException("Addon", "ID", String.valueOf(id));
        }
        return addonsRepository.findByAddonId(id);

    }

    @Override
    public Addon getAddonByName(String name) throws NotFoundException {
        if (!addonsRepository.existsAddonByNameAndEnabledIsTrue(name)) {
            throw new NotFoundException("Addon", "name", name);
        }
        return addonsRepository
                .findAddonByNameAndEnabledIsTrueAndStatus_Status(name, STATUS_APPROVED);
    }

    @Override
    public List<Addon> getAllAddonsOrderByName() {
        return addonsRepository
                .findAllByEnabledIsTrueAndStatus_StatusOrderByNameAsc(STATUS_APPROVED);
    }

    @Override
    public List<Addon> getAllAddonsOrderByDate() {
        return addonsRepository
                .findAllByEnabledIsTrueAndStatus_StatusOrderByUploadDateDesc(STATUS_APPROVED);
    }

    @Override
    public List<Addon> getEightPopularAddons() {
        return addonsRepository
                .findTop8ByEnabledIsTrueAndStatus_StatusOrderByDownloadsDesc(STATUS_APPROVED);
    }

    @Override
    public List<Addon> getFourFeaturedAddons() {
        return addonsRepository
                .findTop4ByEnabledIsTrueAndStatus_StatusOrderByAddonId(STATUS_APPROVED);
    }

    @Override
    public List<Addon> getSixNewestAddons() {
        return addonsRepository
                .findTop6ByEnabledIsTrueAndStatus_StatusOrderByUploadDateDesc(STATUS_APPROVED);
    }

    @Override
    public List<Addon> getAddonsByIde(String ideName) throws NotFoundException {
        if (!idEsRepository.existsByIDEName(ideName)) {
            throw new NotFoundException("IDE", "name", ideName);
        }
        return addonsRepository
                .findAllByEnabledIsTrueAndIde_IDENameAndStatus_Status(ideName, STATUS_APPROVED);
    }

    @Override
    public List<Addon> getAddonsByNumberOfDownloads() {
        return addonsRepository
                .findAllByEnabledIsTrueAndStatus_StatusOrderByDownloadsDesc(STATUS_APPROVED);
    }

    @Override
    public List<Addon> getAddonsByCommitDate() {
        return addonsRepository
                .findAllByEnabledIsTrueAndStatus_StatusOrderByGitHubInfo_LastCommitDate(STATUS_APPROVED);
    }


    @Override
    public Addon deleteAddonById(Addon addon, String username) throws NotAuthorisedException {
        if (!addon.getCreator().getUsername().equals(username) && !Validator.isPrincipalAdmin(username)) {
            throw new NotAuthorisedException(DELETE_AUTHORITIES_CHECK_ERROR);
        }
        Addon deletedAddon = addonsRepository.findByAddonIdAndEnabledIsTrue(addon.getAddonId());
        deletedAddon.setEnabled(false);
        addonsRepository.saveAndFlush(deletedAddon);
        return deletedAddon;
    }

    @Override
    public void createAddon(Addon addon) throws DuplicateException {
        if (addonsRepository.existsAddonByNameAndEnabledIsTrue(addon.getName())) {
            throw new DuplicateException("Addon", "name", addon.getName());
        }

        if (addon.getBinaryFile() != null) {
            this.binaryFilesRepository.save(addon.getBinaryFile());
        }

        addonsRepository.saveAndFlush(addon);
    }

    @Override
    public void updateAddon(Addon addon, String username) throws NotAuthorisedException, NotFoundException, DuplicateException {
        if (!addonsRepository.existsAddonByAddonId(addon.getAddonId())) {
            throw new NotFoundException("Addon", "name", addon.getName());
        }
        if (addonsRepository.existsAddonByNameAndEnabledIsTrue(addon.getName())) {
            throw new DuplicateException("Addon", "name", addon.getName());
        }
        if (!addon.getCreator().getUsername().equals(username) && !Validator.isPrincipalAdmin(username)) {
            throw new NotAuthorisedException(UPDATE_AUTHORITIES_CHECK_ERROR);
        }
        if (addon.getBinaryFile() != null) {
            this.binaryFilesRepository.save(addon.getBinaryFile());
        }
        addonsRepository.saveAndFlush(addon);
    }

    @Override
    public void updateState(Addon addon) throws NotFoundException {
        if (!addonsRepository.existsAddonByAddonId(addon.getAddonId())) {
            throw new NotFoundException("Addon", "name", addon.getName());
        }
        addonsRepository.save(addon);
    }

    @Override
    public void approveAddon(int addonId) throws NotFoundException {
        Addon addon = getAddonById(addonId);

        Status newStatus = addon.getStatus();
        newStatus.setStatus(STATUS_APPROVED);
        newStatus.setStatusId(2);
        addon.setStatus(newStatus);

        addonsRepository.saveAndFlush(addon);
    }
}
