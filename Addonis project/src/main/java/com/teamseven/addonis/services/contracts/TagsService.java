package com.teamseven.addonis.services.contracts;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.Tag;

import java.util.List;
/**
 * This is a class that provides business functionality for the TagsServiceImpl class.
 */
public interface TagsService {

    List<Tag> getAll();

    Tag getTagById(int id) throws NotFoundException;

    Tag getTagByName(String tag) throws NotFoundException;

    void createTag(Tag tag) throws DuplicateException;
}
