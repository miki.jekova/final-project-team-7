package com.teamseven.addonis.services;

import com.teamseven.addonis.models.Addon;
import com.teamseven.addonis.models.Rating;
import com.teamseven.addonis.repositories.contracts.RatingsRepository;
import com.teamseven.addonis.services.contracts.RatingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This is a class that provides implementation of the main functionality of the Rating class.
 */
@Service
public class RatingsServiceImpl implements RatingsService {

    private RatingsRepository ratingsRepository;

    @Autowired
    public RatingsServiceImpl(RatingsRepository ratingsRepository) {
        this.ratingsRepository = ratingsRepository;
    }

    @Override
    public void rateAddon(Rating rating) {
        int addonId = rating.getAddon().getAddonId();
        int userId = rating.getUser().getId();

        if (ratingsRepository.existsByUser_IdAndAddon_AddonId(userId, addonId)) {
            Rating ratingOld = ratingsRepository.findByUser_IdAndAddon_AddonId(userId, addonId);
            ratingOld.setRatingVal(rating.getRatingVal());
            ratingsRepository.save(ratingOld);
        } else {
            ratingsRepository.save(rating);
        }
    }

    @Override
    public void getAddonsRatingById(int id) {
        ratingsRepository.findByRatingId(id);
    }

    @Override
    public Double getAddonsRating(Addon addon) {
        return ratingsRepository.findAverageRatingOfAddon(addon.getAddonId());
    }
}
