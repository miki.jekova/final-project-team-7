package com.teamseven.addonis.services.contracts;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.AppUser;
import com.teamseven.addonis.models.User;

import java.util.List;
/**
 * This interface defines the contract that business functionality classes have to follow.
 */
public interface UsersService {

    List<User> getAll();

    User getByUsername(String username) throws NotFoundException;

    User getById(int id) throws NotFoundException;

    void createUser(User user, AppUser appUser) throws DuplicateException;

    void updateUser(User user, AppUser appUser) throws NotFoundException;

    void deleteUserById(int id) throws NotFoundException;

    void deleteUserByUsername(String username) throws NotFoundException;
}
