package com.teamseven.addonis.services;

import com.teamseven.addonis.models.AppUser;
import com.teamseven.addonis.repositories.contracts.AppUserRepository;
import com.teamseven.addonis.services.contracts.AppUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * This is a class that provides implementation of the main
 * functionality of the User`s additional details class.
 */
@Service
public class AppUsersServiceImpl implements AppUsersService {
    private AppUserRepository appUserRepository;

    @Autowired
    public AppUsersServiceImpl(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public AppUser getById(int id) {
        return appUserRepository.findById(id);
    }
}
