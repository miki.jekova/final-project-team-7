package com.teamseven.addonis.services.contracts;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.IDE;

import java.util.List;
/**
 * This is a class that provides functionality related to the IDEsServiceImpl class.
 */

public interface IDEsService {

    List<IDE> getAll();

    IDE getIdeByID(int id) throws NotFoundException;

    IDE getIdeByName(String name) throws NotFoundException;

    void createIDE(IDE ide) throws DuplicateException;
}
