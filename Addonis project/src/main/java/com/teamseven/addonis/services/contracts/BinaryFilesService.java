package com.teamseven.addonis.services.contracts;

import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.BinaryFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
/**
 * This is a class that provides saving functionality for the BinaryFileServiceImpl class.
 */
public interface BinaryFilesService {
    BinaryFile saveFile(int addonId, MultipartFile multipartFile) throws NotFoundException;
}
