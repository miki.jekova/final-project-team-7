package com.teamseven.addonis.services.contracts;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotAuthorisedException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.Addon;

import java.util.List;

/**
 * "This interface defines the contract that business functionality classes have to follow."
 */
public interface AddonsService {

    List<Addon> findAll();

    List<Addon> findAllPendingAddons();

    Addon getAddonById(int id) throws NotFoundException;

    Addon getAddonByName(String name) throws NotFoundException;

    List<Addon> getAllAddonsOrderByName();

    List<Addon> getAllAddonsOrderByDate();

    List<Addon> getAddonsByCommitDate();

    List<Addon> getEightPopularAddons();

    List<Addon> getFourFeaturedAddons();

    List<Addon> getSixNewestAddons();

    List<Addon> getAddonsByIde(String ideName) throws NotFoundException;

    List<Addon> getAddonsByNumberOfDownloads();

    List<Addon> getAllAddonsOfUser(int userId);

    Addon deleteAddonById(Addon addon, String username) throws NotAuthorisedException;

    void createAddon(Addon addon) throws DuplicateException;

    void updateAddon(Addon addon, String username) throws NotAuthorisedException, NotFoundException, DuplicateException;

    void updateState(Addon addon) throws NotFoundException;

    void approveAddon(int addonId) throws NotFoundException;

}
