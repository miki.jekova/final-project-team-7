package com.teamseven.addonis.services;

import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.Addon;
import com.teamseven.addonis.models.BinaryFile;
import com.teamseven.addonis.repositories.contracts.BinaryFilesRepository;
import com.teamseven.addonis.services.contracts.AddonsService;
import com.teamseven.addonis.services.contracts.BinaryFilesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * This is a class that provides implementation of the main functionality
 * of the BinaryFile class.
 */
@Service
public class BinaryFilesServiceImpl implements BinaryFilesService {
    private AddonsService addonsService;
    private BinaryFilesRepository binaryFilesRepository;

    @Autowired
    public BinaryFilesServiceImpl(AddonsService addonsService,
                                  BinaryFilesRepository binaryFilesRepository) {
        this.addonsService = addonsService;
        this.binaryFilesRepository = binaryFilesRepository;
    }

    @Override
    public BinaryFile saveFile(int addonId, MultipartFile multipartFile) throws NotFoundException {

        try {
            Addon addon = addonsService.getAddonById(addonId);

            BinaryFile binaryFile = new BinaryFile();
            binaryFile.setBinaryFile(multipartFile.getBytes());
            binaryFilesRepository.saveAndFlush(binaryFile);

            addon.setBinaryFile(binaryFile);

            return binaryFile;

        } catch (IOException e) {
            throw new IllegalArgumentException("Invalid operation");
        }
    }
}
