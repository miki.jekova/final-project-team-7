package com.teamseven.addonis.services;

import com.teamseven.addonis.models.Status;
import com.teamseven.addonis.repositories.contracts.StatusRepository;
import com.teamseven.addonis.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This is a class that provides business functionality for
 * getting the Status object from the database by it`s name.
 */
@Service
public class StatusServiceImpl implements StatusService {
    private StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }


    @Override
    public Status getStatusByName(String name) {
        return statusRepository.findByStatus(name);
    }
}
