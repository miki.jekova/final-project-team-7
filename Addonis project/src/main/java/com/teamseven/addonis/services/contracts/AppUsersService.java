package com.teamseven.addonis.services.contracts;

import com.teamseven.addonis.models.AppUser;
/**
 * This is a class that provides business functionality connected with the
 * User`s additional details service implementation class.
 */
public interface AppUsersService {

    AppUser getById(int id);
}
