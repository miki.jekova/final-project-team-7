package com.teamseven.addonis.builders;

/**
 * This is a class that accepts an input that represents url repository
 * and returns a converted url representing a github api resource.
 */
public class URLBuilder {
    public static String getGitHubLocation(String repositoryUrl) {
        String[] arr = repositoryUrl.split(".com/");
        return "https://api.github.com/repos/" + arr[1];
    }

}
