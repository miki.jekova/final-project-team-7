package com.teamseven.addonis.constants;

public class Constants {

    public static final String GITHUB_COMMITS = "/commits";
    public static final String GITHUB_OPEN_ISSUES_COUNT = "open_issues_count";
    public static final String GITHUB_PULLS = "/pulls";
    public static final String STATUS_APPROVED = "approved";
    public static final String STATUS_PENDING = "pending";
    public static final String EMPTY_FILE_FIELD_ERROR = "You have to upload the file!";
    public static final String DELETE_AUTHORITIES_CHECK_ERROR = "You are not authorised to delete this addon!";
    public static final String UPDATE_AUTHORITIES_CHECK_ERROR = "You are not authorised to update this addon!";
    public static final String CREATE_AUTHORITIES_CHECK_ERROR = "You must be registered user to create addon!";
    public static final String ADDON_DESCRIPTION_CHECK_ERROR = "You need to write a description of the addon with minimum 4 characters!";
    public static final String ADDON_ORIGIN_LINK_CHECK_ERROR = "You need to set a origin link!";
    public static final String ADDON_NAME_CHECK_ERROR = "You need to write a name of the addon wit minimum 4 characters!";
    public static final String EXCEPTION_MESSAGE = "Sorry something went wrong! Please try again!";
}
