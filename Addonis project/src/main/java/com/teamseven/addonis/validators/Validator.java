package com.teamseven.addonis.validators;

import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.DTOs.AddonDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import static com.teamseven.addonis.constants.Constants.*;
import static com.teamseven.addonis.constants.Constants.ADDON_ORIGIN_LINK_CHECK_ERROR;

@Component
public class Validator {

    private static UserDetailsManager userDetailsManager;

    @Autowired
    public Validator(UserDetailsManager userDetailsManager) {
        Validator.userDetailsManager = userDetailsManager;
    }

    public static boolean isPrincipalAdmin(String username) {
        if (userDetailsManager.userExists(username)) {
            Object[] roles = userDetailsManager.loadUserByUsername(username).getAuthorities().toArray();
            for (Object role : roles) {
                if (role.toString().equals("ROLE_ADMIN")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void checkUserInput(AddonDTO addonDTO, MultipartFile file) throws NotFoundException {

        if (addonDTO.getName() == null || addonDTO.getName().length() < 4) {
            throw new IllegalArgumentException(ADDON_NAME_CHECK_ERROR);
        }

        if (addonDTO.getDescription() == null || addonDTO.getDescription().length() < 4) {
            throw new IllegalArgumentException(ADDON_DESCRIPTION_CHECK_ERROR);
        }

        if (file.isEmpty()){
            throw new NotFoundException(EMPTY_FILE_FIELD_ERROR);
        }

        if (addonDTO.getOriginLink() == null) {
            throw new IllegalArgumentException(ADDON_ORIGIN_LINK_CHECK_ERROR);
        }
    }
}
