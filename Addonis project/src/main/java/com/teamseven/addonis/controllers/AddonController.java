package com.teamseven.addonis.controllers;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotAuthorisedException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.*;
import com.teamseven.addonis.models.DTOs.AddonDTO;
import com.teamseven.addonis.models.DTOs.AddonUpdateDTO;
import com.teamseven.addonis.models.DTOs.RatingDTO;
import com.teamseven.addonis.services.contracts.*;
import com.teamseven.addonis.services.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import static com.teamseven.addonis.constants.Constants.*;

/**
 * This is a class that maps model attributes relating with the Addon class to the view.
 */
@Controller
@RequestMapping("/addon")
public class AddonController {

    private AddonsService addonsService;
    private Mapper mapper;
    private StatusService statusService;
    private UsersService usersService;
    private IDEsService idesService;
    private TagsService tagsService;
    private RatingsService ratingsService;

    @Autowired
    public AddonController(AddonsService addonsService,
                           Mapper mapper,
                           StatusService statusService,
                           UsersService usersService,
                           IDEsService idesService,
                           TagsService tagsService,
                           RatingsService ratingsService) {
        this.addonsService = addonsService;
        this.mapper = mapper;
        this.statusService = statusService;
        this.usersService = usersService;
        this.idesService = idesService;
        this.tagsService = tagsService;
        this.ratingsService = ratingsService;
    }

    @GetMapping("/{id}")
    public String showAddonById(@ModelAttribute RatingDTO ratingDTO, @PathVariable int id, Model model, Principal principal) throws NotFoundException {
        Addon addon = addonsService.getAddonById(id);
        if (principal != null) {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));

            if (principal.getName().equals(addon.getCreator().getAppUser().getName())) {
                model.addAttribute("creator", principal);
            }
        }
        if (ratingsService.getAddonsRating(addon) != null) {
            ratingDTO.setAvgRating(ratingsService.getAddonsRating(addon));
        }

        List<Tag> tags = tagsService.getAll();
        model.addAttribute("addon", addon);
        model.addAttribute("ratingDTO", ratingDTO);
        model.addAttribute("allTags", tags);
        return "addon-details";
    }

    @PostMapping("/create")
    public String createAddon(@ModelAttribute AddonDTO addonDTO,
                              Principal principal,
                              @RequestParam("binaryfile") MultipartFile file,
                              Model model) {

        try {

            Addon addonToCreate = mapper.toAddon(addonDTO, file);
            addonToCreate.setUploadDate(new Date());
            addonToCreate.setEnabled(true);

            User creator = usersService.getByUsername(principal.getName());

            addonToCreate.setCreator(creator);
            addonToCreate.setStatus(statusService.getStatusByName(STATUS_PENDING));
            addonsService.createAddon(addonToCreate);
            model.addAttribute("addonDTO", addonDTO);

            return "redirect:/addon/" + addonToCreate.getAddonId();
        } catch (DuplicateException | NotFoundException | IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "create-addon";
        } catch (Exception e) {
            model.addAttribute("error", EXCEPTION_MESSAGE);
            return "create-addon";
        }
    }

    @GetMapping("/create")
    public String createAddon(Model model) {
        model.addAttribute("addonDTO", new AddonDTO());
        return "create-addon";
    }

    @ModelAttribute("ides")
    public List<IDE> fillingIDE() {
        return idesService.getAll();
    }

    @PostMapping("/update/{addonId}")
    public String updateAddon(@ModelAttribute AddonUpdateDTO addonUpdateDTO,
                              @PathVariable int addonId,
                              Principal principal,
                              Model model) {
        try {
            Addon oldAddon = addonsService.getAddonById(addonId);

            Addon addonToUpdate = mapper.mergeAddons(oldAddon, addonUpdateDTO);
            addonsService.updateAddon(addonToUpdate, principal.getName());

            model.addAttribute("addonUpdateDTO", addonUpdateDTO);

            return "redirect:/addon/" + oldAddon.getAddonId();
        } catch (NotFoundException | NotAuthorisedException | DuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "update-addon";
        } catch (Exception e) {
            model.addAttribute("error", EXCEPTION_MESSAGE);
            return "update-addon";
        }

    }

    @GetMapping("/update/{addonId}")
    public String updateAddon(Model model, @PathVariable int addonId) {
        model.addAttribute("addonUpdateDTO", new AddonUpdateDTO());
        return "update-addon";
    }

    @PostMapping("/rate/{id}")
    public String rateAddon(@ModelAttribute RatingDTO ratingDTO,
                            @PathVariable int id,
                            Principal principal,
                            Model model) throws NotFoundException {

        model.addAttribute("ratingDTO", ratingDTO);
        Rating rating = mapper.toRating(id, ratingDTO, principal.getName());
        ratingsService.rateAddon(rating);

        return "redirect:/addon/{id}";
    }

    @RequestMapping("/delete/{addonId}")
    public String deleteAddon(@PathVariable int addonId, Principal principal, Model model) {

        try {
            Addon addonToDelete = addonsService.getAddonById(addonId);

            if (principal != null) {
                addonsService.deleteAddonById(addonToDelete, principal.getName());
            }

            return "redirect:/";

        } catch (NotFoundException | NotAuthorisedException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/";
        } catch (Exception e) {
            model.addAttribute("error", EXCEPTION_MESSAGE);
            return "redirect:/";
        }
    }
}
