package com.teamseven.addonis.controllers;

import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.Addon;
import com.teamseven.addonis.services.contracts.AddonsService;
import com.teamseven.addonis.services.contracts.UsersService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * This is a class that maps model attributes relating to the admin role of the user class
 * to the view.
 */

@Controller
@RequestMapping("/admin")
public class AdminController {
    UsersService usersService;
    AddonsService addonsService;

    public AdminController(UsersService usersService, AddonsService addonsService) {
        this.usersService = usersService;
        this.addonsService = addonsService;
    }

    @GetMapping
    public String showAdminPage(Model model, Principal principal){
        try {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));
            model.addAttribute("pendingAddons", addonsService.findAllPendingAddons());
        } catch (Exception e) {
            return "redirect:/";
        }
        return "admin";
    }
    @GetMapping("/access-denied")
    public String showAccessDenied(){
        return "access-denied";
    }

    @GetMapping("/users")
    public String getAllUsers(Model model, Principal principal) {
        try {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));
            model.addAttribute("users", usersService.getAll());
        } catch (Exception e) {
            return "redirect:/logout";
        }
        return "admin-users";
    }

    @GetMapping("/pending")
    public String getAllPendingAddons(Model model, Principal principal) {
        try {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));
            model.addAttribute("pendingAddons", addonsService.findAllPendingAddons());
        } catch (Exception e) {
            return "redirect:/admin";
        }
        return "admin-pending";
    }

    @PostMapping("/approve/{addonId}")
    public String approveAddon(@PathVariable("addonId") int addonId, Model model, Principal principal) {
        try {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));
            addonsService.approveAddon(addonId);
        } catch (Exception e) {
            return "redirect:/admin";
        }
        return "redirect:/admin/pending";
    }
}
