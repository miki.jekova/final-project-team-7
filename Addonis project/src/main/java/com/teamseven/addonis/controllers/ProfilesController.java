package com.teamseven.addonis.controllers;

import com.teamseven.addonis.models.User;
import com.teamseven.addonis.services.contracts.AddonsService;
import com.teamseven.addonis.services.contracts.UsersService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;

/**
 * This is a class that shows the user`s functionality and fields.
 */

@Controller
@RequestMapping("/users")
public class ProfilesController {
    UsersService usersService;
    AddonsService addonsService;

    public ProfilesController(UsersService usersService, AddonsService addonsService) {
        this.usersService = usersService;
        this.addonsService = addonsService;
    }

    @GetMapping("/{username}")
    public String getSingleUser(@PathVariable("username") String username, Model model) {
        User user = new User();
        try {
            user = usersService.getByUsername(username);
            model.addAttribute("user", user);
            model.addAttribute("myAddons", addonsService.getAllAddonsOfUser(user.getId()));
        } catch (Exception e) {
            if (!user.getUsername().isEmpty()) {
                return "redirect:/";
            }
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "User Not Found", e);
        }
        return "profile";
    }

    @PostMapping("/delete/{username}")
    public String deleteUser(Model model, @PathVariable("username") String username, Principal principal) {
        try {
            usersService.deleteUserByUsername(username);
        } catch (Exception e) {
            if (principal == null) {
                return "redirect:/";
            }
            model.addAttribute("error", e.getMessage());
            return "redirect:/admin/users";
        }
        return "redirect:/admin/users";
    }
}
