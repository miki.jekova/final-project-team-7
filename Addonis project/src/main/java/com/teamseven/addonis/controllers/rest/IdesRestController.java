package com.teamseven.addonis.controllers.rest;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.IDE;
import com.teamseven.addonis.services.contracts.IDEsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * This is a class that maps HTTP requests relating with the IDE class to the functions below.
 */
@RestController
@RequestMapping("/api/ides")
public class IdesRestController {

    private final IDEsService idEsService;

    @Autowired
    public IdesRestController(IDEsService idEsService) {
        this.idEsService = idEsService;
    }

    @GetMapping
    public List<IDE> getAll() {
        return idEsService.getAll();
    }

    @GetMapping("/id")
    public IDE getIdeByID(@RequestParam int id) {
        try {
            return idEsService.getIdeByID(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/name")
    public IDE getIdeByName(@RequestParam String name) {
        try {
            return idEsService.getIdeByName(name);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/create")
    public IDE createIDE(@RequestParam String name) {
        try {
            IDE newIDE = new IDE();
            newIDE.setIDEName(name);
            idEsService.createIDE(newIDE);
            return newIDE;
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
