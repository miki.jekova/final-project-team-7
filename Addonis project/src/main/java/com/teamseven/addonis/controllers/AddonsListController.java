package com.teamseven.addonis.controllers;

import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.Addon;
import com.teamseven.addonis.models.IDE;
import com.teamseven.addonis.services.contracts.AddonsService;
import com.teamseven.addonis.services.contracts.IDEsService;
import com.teamseven.addonis.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Executable;
import java.security.Principal;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;

@Controller
public class AddonsListController {

    private AddonsService addonsService;
    private UsersService usersService;
    private IDEsService idEsService;

    @Autowired
    public AddonsListController(AddonsService addonsService,
                                IDEsService idEsService, UsersService usersService) {
        this.addonsService = addonsService;
        this.idEsService = idEsService;
        this.usersService = usersService;
    }

    @GetMapping("/addons")
    public String getAddons(Model model, Principal principal) throws NotFoundException {
        List<Addon> list = addonsService.findAll();
        model.addAttribute("addons", list);
        if (principal != null) {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));
        }
        return "addons-list";
    }

    @GetMapping("/addons/sort/name")
    public String getSortedByName(Model model, Principal principal) throws NotFoundException {
        List<Addon> list = addonsService.getAllAddonsOrderByName();
        model.addAttribute("addons", list);
        if (principal != null) {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));
        }
        return "addons-list";
    }

    @GetMapping("/addons/sort/downloads")
    public String getSortedByDownloads(Model model, Principal principal) throws NotFoundException {
        List<Addon> list = addonsService.getAddonsByNumberOfDownloads();
        model.addAttribute("addons", list);
        if (principal != null) {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));
        }
        return "addons-list";
    }

    @GetMapping("/addons/sort/upload-date")
    public String getSortedByUploadDate(Model model, Principal principal) throws NotFoundException {
        List<Addon> list = addonsService.getAllAddonsOrderByDate();
        model.addAttribute("addons", list);
        if (principal != null) {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));
        }
        return "addons-list";
    }

    @GetMapping("/addons/sort/last-commit-date")
    public String getSortedByLastCommitDate(Model model, Principal principal) throws NotFoundException {
        List<Addon> list = addonsService.getAddonsByCommitDate();
        model.addAttribute("addons", list);
        if (principal != null) {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));
        }
        return "addons-list";
    }

    @GetMapping("/addons/filter/ide")
    public String getFilteredByIde(Model model, IDE ide, Principal principal) throws NotFoundException {
        List<Addon> list = addonsService.getAddonsByIde(ide.getIDEName());
        model.addAttribute("addons", list);
        model.addAttribute("ideName", ide.getIDEName());
        if (principal != null) {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));
        }
        return "addons-list";
    }

    @GetMapping("/addons/filter/addonName")
    public String getFilteredByName(Model model, Addon addon, Principal principal) throws NotFoundException {
        if (principal != null) {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));
        }
        List<Addon> list = new ArrayList<>();
        try {
            Addon addonByName = addonsService.getAddonByName(addon.getName());
            list.add(addonByName);
        } catch (Exception ignored) {

        }
        model.addAttribute("addons", list);
        return "addons-list";
    }

    @ModelAttribute("ides")
    public List<IDE> fillingIDEDropDown() {
        return idEsService.getAll();
    }
}
