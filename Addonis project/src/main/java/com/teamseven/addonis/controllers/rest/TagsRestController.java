package com.teamseven.addonis.controllers.rest;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.Tag;
import com.teamseven.addonis.services.contracts.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * This is a class that maps HTTP requests relating with the Tag class to the functions below.
 */
@RestController
@RequestMapping("/api/tags")
public class TagsRestController {

    private final TagsService tagsService;

    @Autowired
    public TagsRestController(TagsService tagsService) {
        this.tagsService = tagsService;
    }

    @GetMapping
    public List<Tag> getAllTags() {
        return tagsService.getAll();
    }

    @GetMapping("/id")
    public Tag getTagById(@RequestParam int id) {
        try {
            return tagsService.getTagById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/name")
    public Tag getTagByName(@RequestParam String name) {
        try {
            return tagsService.getTagByName(name);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Tag createTag(@RequestParam String name) {
        try {
            Tag newTag = new Tag();
            newTag.setTag(name);
            tagsService.createTag(newTag);
            return newTag;
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
