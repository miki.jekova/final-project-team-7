package com.teamseven.addonis.controllers;

import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.Addon;
import com.teamseven.addonis.models.Tag;
import com.teamseven.addonis.services.contracts.AddonsService;
import com.teamseven.addonis.services.contracts.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TagController {
    private TagsService tagsService;
    private AddonsService addonsService;

    @Autowired
    public TagController(TagsService tagsService, AddonsService addonsService) {
        this.tagsService = tagsService;
        this.addonsService = addonsService;
    }

    @GetMapping("/addon/{id}/add-tag")
    public String tagAddonForm(@PathVariable int id, Model model) throws NotFoundException {

        Addon addon = addonsService.getAddonById(id);
        model.addAttribute("allTags", tagsService.getAll());
        model.addAttribute("addon", addon);
        model.addAttribute("tagDto", new Tag());
        return "addon-details";
    }

    @PostMapping("/addon/{id}/tag")
    public String tagAddon(@PathVariable("id") int id, @ModelAttribute("tagDto") Tag tag) throws NotFoundException {

        Addon addonToTag = addonsService.getAddonById(id);
        Tag tagDB = tagsService.getTagByName(tag.getTag());
        addonToTag.addTag(tagDB);
        addonsService.updateState(addonToTag);
        return "redirect:/addon/" + addonToTag.getAddonId();
    }
}
