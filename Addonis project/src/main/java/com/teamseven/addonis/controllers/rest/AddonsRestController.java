package com.teamseven.addonis.controllers.rest;

import com.teamseven.addonis.exceptions.DuplicateException;
import com.teamseven.addonis.exceptions.NotAuthorisedException;
import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.*;
import com.teamseven.addonis.models.DTOs.AddonDTO;
import com.teamseven.addonis.models.DTOs.AddonUpdateDTO;
import com.teamseven.addonis.services.contracts.*;
import com.teamseven.addonis.services.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import static com.teamseven.addonis.constants.Constants.*;

/**
 * This is a class that maps HTTP requests relating with the Addon class to the functions below.
 */
@RestController
@RequestMapping("/api/addons")
public class AddonsRestController {

    private AddonsService addonsService;
    private RatingsService ratingsService;
    private UsersService usersService;
    private Mapper mapper;
    private StatusService statusService;

    @Autowired
    public AddonsRestController(AddonsService addonsService,
                                RatingsService ratingsService,
                                UsersService usersService,
                                Mapper mapper,
                                StatusService statusService) {
        this.addonsService = addonsService;
        this.ratingsService = ratingsService;
        this.usersService = usersService;
        this.mapper = mapper;
        this.statusService = statusService;
    }

    @GetMapping("/{id}")
    public Addon getAddonById(@PathVariable int id) {
        try {
            return addonsService.getAddonById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/name")
    public Addon getAddonByName(@RequestParam String name) {
        try {
            return addonsService.getAddonByName(name);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/all")
    public List<Addon> findAll() {
        return addonsService.findAll();
    }

    @GetMapping("/all/pending")
    public List<Addon> findAllPending() {
        return addonsService.findAllPendingAddons();
    }

    @GetMapping("/all/{userId}")
    public List<Addon> getAllAddonsOfUser(@PathVariable int userId) {
        return addonsService.getAllAddonsOfUser(userId);
    }

    @GetMapping("/all/name")
    public List<Addon> getAllAddonsOrderByName() {
        return addonsService.getAllAddonsOrderByName();
    }

    @GetMapping("/all/date")
    public List<Addon> getAllAddonsOrderByDate() {
        return addonsService.getAllAddonsOrderByDate();
    }

    @GetMapping("/popular")
    public List<Addon> getEightPopularAddons() {
        return addonsService.getEightPopularAddons();
    }

    @GetMapping("/featured")
    public List<Addon> getFourFeaturedAddons() {
        return addonsService.getFourFeaturedAddons();
    }

    @GetMapping("/newest")
    public List<Addon> getSixNewestAddons() {
        return addonsService.getSixNewestAddons();
    }

    @GetMapping("/ide")
    public List<Addon> getAddonsByIde(@RequestParam String name) {
        try {
            return addonsService.getAddonsByIde(name);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/downloads")
    public List<Addon> getAddonsByDownloads() {
        return addonsService.getAddonsByNumberOfDownloads();
    }

    @GetMapping("/all/commit")
    public List<Addon> getAddonsByCommitDateAsc() {
        return addonsService.getAddonsByCommitDate();
    }


    @GetMapping("/{id}/rating")
    public double getAddonsRating(@PathVariable int id) {
        Addon addon = getAddonById(id);
        try {
            return ratingsService.getAddonsRating(addon);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Addon createAddon(@ModelAttribute AddonDTO addonDTO,
                             @RequestParam("file") MultipartFile file,
                             Principal principal) throws IOException, NotFoundException {

        if (principal == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, CREATE_AUTHORITIES_CHECK_ERROR);
        }
        try {
            Addon addonToCreate = mapper.toAddon(addonDTO, file);
            addonToCreate.setUploadDate(new Date());
            addonToCreate.setEnabled(true);

            User creator = usersService.getByUsername(principal.getName());
            addonToCreate.setCreator(creator);

            addonToCreate.setStatus(statusService.getStatusByName(STATUS_PENDING));

            addonsService.createAddon(addonToCreate);
            return addonToCreate;
        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Addon updateAddon(AddonUpdateDTO updateDTO, @PathVariable int id, Principal principal) throws NotFoundException {
        Addon oldAddon = addonsService.getAddonById(id);
        if (principal == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, UPDATE_AUTHORITIES_CHECK_ERROR);
        }
        Addon addonToUpdate = mapper.mergeAddons(oldAddon, updateDTO);
        try {
            addonsService.updateAddon(addonToUpdate, principal.getName());
            return addonToUpdate;
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotAuthorisedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Addon deleteAddon(@PathVariable int id, Principal principal) throws NotFoundException {
        Addon addonToDelete = addonsService.getAddonById(id);
        if (principal == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        try {
            return addonsService.deleteAddonById(addonToDelete, principal.getName());
        } catch (NotAuthorisedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/download")
    public ResponseEntity<Resource> getAddonBinary(@PathVariable int id) throws NotFoundException {
        Addon addon = addonsService.getAddonById(id);

        addon.setDownloads(addon.getDownloads() + 1);
        addonsService.updateState(addon);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"fileName.exe\"")
                .body(new ByteArrayResource(addon.getBinaryFile().getBinaryFile()));
    }
}
