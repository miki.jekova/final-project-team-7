package com.teamseven.addonis.controllers.rest;

import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.AppUser;
import com.teamseven.addonis.models.DTOs.UserDTO;
import com.teamseven.addonis.models.User;
import com.teamseven.addonis.services.contracts.AppUsersService;
import com.teamseven.addonis.services.contracts.UsersService;
import com.teamseven.addonis.services.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

/**
 * This is a class that maps HTTP requests relating with the User class to the functions below.
 */
@RestController
@RequestMapping("/api/users")
public class UsersRestController {
    private UsersService usersService;
    private Mapper mapper;
    private AppUsersService appUsersService;

    @Autowired
    public UsersRestController(UsersService usersService,
                               Mapper mapper,
                               AppUsersService appUsersService) {
        this.usersService = usersService;
        this.mapper = mapper;
        this.appUsersService = appUsersService;
    }

    @GetMapping
    public List<User> getAll() {
        return usersService.getAll();
    }

    @GetMapping("/name")
    public User getByUsername(@RequestParam String username) {
        try {
            return usersService.getByUsername(username);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public User getAppUserById(@PathVariable int id) {
        try {
            return usersService.getById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable int id,
                           @ModelAttribute @Valid UserDTO userDTO) {
        try {
            User userToUpdate = usersService.getById(id);
            AppUser appUser = appUsersService.getById(id);
            userToUpdate = mapper.mergeUsers(appUser, userToUpdate, userDTO);
            usersService.updateUser(userToUpdate, appUser);
            return userToUpdate;
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @DeleteMapping("/{id}")
    public void deleteAppUser(@PathVariable int id) {
        try {
            usersService.deleteUserById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
