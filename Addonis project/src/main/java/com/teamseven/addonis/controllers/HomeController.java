package com.teamseven.addonis.controllers;

import com.teamseven.addonis.exceptions.NotFoundException;
import com.teamseven.addonis.models.Addon;
import com.teamseven.addonis.services.contracts.AddonsService;
import com.teamseven.addonis.services.contracts.UsersService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;

/**
 * This is a class that maps model attributes relating with the Addon, User classes to the view,
 * Also it visualise logging and register functionality.
 */
@Controller
public class HomeController {
    private AddonsService addonsService;
    private UsersService usersService;

    public HomeController(AddonsService addonsService, UsersService usersService) {
        this.addonsService = addonsService;
        this.usersService = usersService;
    }

    @GetMapping("/")
    public String showAddons(Model model, Principal principal) throws NotFoundException {
        getNamePrincipal(model, principal);
        model.addAttribute("featuredAddons", addonsService.getFourFeaturedAddons());
        model.addAttribute("newestAddons", addonsService.getSixNewestAddons());
        model.addAttribute("popularAddons", addonsService.getEightPopularAddons());
        return "index";
    }

    private void getNamePrincipal(Model model, Principal principal) throws NotFoundException {
        if (principal != null) {
            model.addAttribute("user", usersService.getByUsername(principal.getName()));
        }
    }

    @GetMapping("/addons/{addonId}")
    public String showAddon(@PathVariable("addonId") int addonId, Model model) {
        Addon addon = new Addon();
        try {
            addon = addonsService.getAddonById(addonId);
            model.addAttribute("addon", addon);
        } catch (Exception e) {
            if (!addon.isEnabled()) {
                return "index";
            }
        }
        return "index";
    }
}
