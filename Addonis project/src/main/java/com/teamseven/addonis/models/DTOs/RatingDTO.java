package com.teamseven.addonis.models.DTOs;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class is used when registered users or admins rate an existing addon.
 * It represents the rating number given.
 */

@Getter
@Setter
@NoArgsConstructor
public class RatingDTO {
    private int rating;
    private double avgRating = 0.0;

}
