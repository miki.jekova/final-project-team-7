package com.teamseven.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * This class is used for storing the date, when each of the commits are posted.
 */

@Getter
@Setter
@NoArgsConstructor
public class GitHubCommit {
    private String title;
    private Date date;
}
