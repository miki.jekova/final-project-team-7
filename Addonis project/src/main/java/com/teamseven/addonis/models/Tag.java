package com.teamseven.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * This class is used for creating/updating/deleting tags of existing addon.
 */
@Entity
@Table(name = "tags")
@NoArgsConstructor
@Getter
@Setter
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tag_id")
    private int tagId;

    @Size(min = 2, max = 50, message = "Tag must be between 2 and 50 characters!")
    @Column(name = "tag")
    private String tag;
}
