package com.teamseven.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * This class gives the ability to create Addon, which is the main
 * unit it this project and later on to be
 * downloaded/sorted/filtered/rated
 */

@Entity
@Table(name = "addons")
@NoArgsConstructor
@Getter
@Setter
public class Addon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "addon_id")
    private int addonId;

    @NotBlank
    @Column(name = "name")
    @Size(min = 4, max = 50, message = "Addon's name must be between 4 and 50 characters!")
    private String name;

    @NotBlank
    @Column(name = "description")
    private String description;

    @Column(name = "downloads")
    private int downloads;

    @OneToOne
    @JoinColumn(name = "file_id")
    private BinaryFile binaryFile;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private User creator;

    @Column(name = "upload_date")
    private Date uploadDate;

    @Column(name = "origin_link")
    private String originLink;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "addons_tags_table",
            joinColumns = @JoinColumn(name = "addon_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tag> tags = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "IDE_id")
    private IDE ide;

    @OneToOne()
    @JoinColumn(name = "git_hub_info_id")
    private GitHubInfo gitHubInfo;
    
    @Column(name = "enabled")
    private boolean enabled;

    public void addTag(Tag tag){
        tags.add(tag);
    }
}
