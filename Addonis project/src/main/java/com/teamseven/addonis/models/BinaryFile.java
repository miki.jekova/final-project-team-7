package com.teamseven.addonis.models;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Thanks to this class users are able to transfer the addons as binary file
 * and upload them in the database from where other users can download them.
 */

@Entity
@Table(name = "files")
@NoArgsConstructor
@Getter
@Setter
public class BinaryFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "file_id")
    private int id;

    @Column(name = "file")
    @Lob
    private byte[] binaryFile;
}
