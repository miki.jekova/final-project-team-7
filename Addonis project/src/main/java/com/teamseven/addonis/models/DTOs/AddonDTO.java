package com.teamseven.addonis.models.DTOs;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * This class is used to transfer data to create an Addon class object.
 * The data is provided by a registered user or admin.
 */

@Getter
@Setter
@NoArgsConstructor
public class AddonDTO {

    @NotBlank
    @Size(min = 4, max = 50, message = "Addon's name must be between 5 and 50 characters!")
    private String name;

    @NotBlank
    @Size(min = 4, message = "Addon's description must be minimum 4 characters!")
    private String description;

    @NotBlank
    private String originLink;

    @Size(min = 5, max = 30, message = "IDE must be between 5 and 30 characters!")
    private String IDE;

}
