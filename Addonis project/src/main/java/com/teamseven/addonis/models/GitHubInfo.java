package com.teamseven.addonis.models;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * This class is used for getting additional information for the addon.
 * As issues count, pull requests count, the date/title of the last commit
 * from the GitHub repository of the addon.
 */

@Entity
@Table(name = "github_info")
@NoArgsConstructor
@Getter
@Setter
public class GitHubInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "github_id")
    private int id;

    @Column(name = "issues_count")
    private int issuesCount;

    @Column(name = "pull_requests_count")
    private int pullRequestsCount;

    @Column(name = "last_commit_date")
    private Date lastCommitDate;

    @Column(name = "last_commit_title")
    private String lastCommitTitle;
}
