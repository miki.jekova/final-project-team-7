package com.teamseven.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * With this class we are able to store the different types of IDEs
 * and use them to filter addons by them.
 */
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "ides")
public class IDE {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ide_id")
    private int IDEId;

    @Size(min = 2, max = 50, message = "IDE must be between 2 and 50 characters!")
    @Column(name = "name")
    private String IDEName;
}
