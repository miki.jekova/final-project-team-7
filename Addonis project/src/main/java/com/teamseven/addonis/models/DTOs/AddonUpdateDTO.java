package com.teamseven.addonis.models.DTOs;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * This class is used to transfer data to update an existing Addon class object.
 * The data is provided by a registered user or admin.
 */

@Getter
@Setter
@NoArgsConstructor
public class AddonUpdateDTO {

    @Size(min = 5, max = 50, message = "Addon's name must be between 5 and 50 characters!")
    private String name;

    @Size(min = 5, max = 1000, message = "Addon's description must be between 5 and 1000 characters!")
    private String description;

    @Size(min = 2, max = 50, message = "IDE must be between 5 and 30 characters!")
    private String IDE;

}
