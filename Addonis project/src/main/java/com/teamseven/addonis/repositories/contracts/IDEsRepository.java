package com.teamseven.addonis.repositories.contracts;

import com.teamseven.addonis.models.IDE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * This is a class that provides functionality connected with the IDE class.
 */
@Repository
public interface IDEsRepository extends JpaRepository<IDE, Integer> {

    List<IDE> findAll();

    IDE findIDEByIDEId(int id);

    IDE findIDEByIDEName(String name);

    boolean existsByIDEName(String name);

    boolean existsByIDEId(int id);
}
