package com.teamseven.addonis.repositories.contracts;

import com.teamseven.addonis.models.GitHubInfo;
/**
 * This is a class that provides saving functionality for the GitHubClientServiceImpl class.
 */
public interface GitHubClientRepository {
    GitHubInfo getRepositoryInfo(String repositoryUrl);
}
