package com.teamseven.addonis.repositories.contracts;

import com.teamseven.addonis.models.AppUser;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
/**
 * This interface serves to retrieve database records for the User's additional details class.
 */
public interface AppUserRepository extends JpaRepository<AppUser, Integer> {

    AppUser findById(int id);

    @NotNull
    List<AppUser> findAll();
}
