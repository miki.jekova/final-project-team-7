package com.teamseven.addonis.repositories;

import com.teamseven.addonis.models.GitHubCommit;
import com.teamseven.addonis.models.GitHubInfo;
import com.teamseven.addonis.repositories.contracts.GitHubClientRepository;
import com.teamseven.addonis.builders.URLBuilder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;

import static com.teamseven.addonis.constants.Constants.*;

/**
 * This is a class that provides business functionality for the GitHub connection.
 */
@Service
public class GitHubClientRepositoryImpl implements GitHubClientRepository {

    private String init(HttpMethod verb, String url) throws IOException {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();

        Request request = new Request.Builder()
                .url(URLBuilder.getGitHubLocation(url))
                .method(verb.toString(), null)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    @Override
    public GitHubInfo getRepositoryInfo(String repositoryUrl) throws IllegalArgumentException {
        try {
            JSONObject responseBodyJSON = new JSONObject(init(HttpMethod.GET, repositoryUrl));

            GitHubInfo githubInfo = new GitHubInfo();
            githubInfo.setIssuesCount(responseBodyJSON.getInt(GITHUB_OPEN_ISSUES_COUNT));
            int pullRequestsCount = getPullRequestsCount(repositoryUrl);
            githubInfo.setPullRequestsCount(pullRequestsCount);
            GitHubCommit lastCommit = getLastCommit(repositoryUrl);
            if (lastCommit != null) {
                githubInfo.setLastCommitDate(lastCommit.getDate());
                githubInfo.setLastCommitTitle(lastCommit.getTitle());
            }
            return githubInfo;
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    private int getPullRequestsCount(String repositoryUrl) {

        try {
            JSONArray responseBodyJSON = new JSONArray(init(HttpMethod.GET, repositoryUrl + GITHUB_PULLS));

            return responseBodyJSON.length();
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    private GitHubCommit getLastCommit(String repositoryUrl) {

        try {

            JSONArray responseBodyJSON = new JSONArray(init(HttpMethod.GET, repositoryUrl + GITHUB_COMMITS));

            boolean isEmptyRepository = responseBodyJSON.length() == 0;
            if (isEmptyRepository) {
                return null;
            }

            GitHubCommit lastCommit = new GitHubCommit();
            Date lastCommitDate = Date.from(Instant.parse(responseBodyJSON
                    .getJSONObject(0)
                    .getJSONObject("commit")
                    .getJSONObject("author")
                    .getString("date")));
            lastCommit.setDate(lastCommitDate);
            lastCommit.setTitle(responseBodyJSON
                    .getJSONObject(0)
                    .getJSONObject("commit")
                    .getString("message"));

            return lastCommit;
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }
}
