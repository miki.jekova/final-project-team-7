package com.teamseven.addonis.repositories.contracts;

import com.teamseven.addonis.models.Addon;
import com.teamseven.addonis.models.IDE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * "This interface serves to retrieve database records for the Addon class."
 */
@Repository
public interface AddonsRepository extends JpaRepository<Addon, Integer> {

    Addon findByAddonId(int id);

    Addon findByAddonIdAndEnabledIsTrue(int id);

    List<Addon> findAllByEnabledIsTrueAndStatus_Status(String statusName);

    Addon findAddonByNameAndEnabledIsTrueAndStatus_Status(String addonName, String statusName);

    List<Addon> findAllByEnabledIsTrueAndStatus_StatusOrderByNameAsc(String statusName);

    List<Addon> findAllByEnabledIsTrueAndStatus_StatusOrderByUploadDateDesc(String statusName);

    List<Addon> findTop8ByEnabledIsTrueAndStatus_StatusOrderByDownloadsDesc(String statusName);

    List<Addon> findTop4ByEnabledIsTrueAndStatus_StatusOrderByAddonId(String statusName);

    List<Addon> findTop6ByEnabledIsTrueAndStatus_StatusOrderByUploadDateDesc(String statusName);

    List<Addon> findAllByEnabledIsTrueAndIde_IDENameAndStatus_Status(String ideName, String statusName);

    List<Addon> findAllByEnabledIsTrueAndStatus_StatusOrderByDownloadsDesc(String string);

    List<Addon> findAllByEnabledIsTrueAndStatus_StatusOrderByGitHubInfo_LastCommitDate(String statusName);

    List<Addon> findAllByEnabledIsTrueAndCreator_Id(int userId);

    List<Addon> findAddonsByEnabledIsTrueAndStatus_StatusAndCreator_Username(String statusPending, String creatorUsername);

    boolean existsAddonByNameAndEnabledIsTrue(String name);

    boolean existsAddonByAddonIdAndEnabledIsTrue(int id);

    boolean existsAddonByAddonId(int id);

}
