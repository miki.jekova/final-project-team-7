package com.teamseven.addonis.repositories.contracts;

import com.teamseven.addonis.models.GitHubInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * This is a class that is used for saving the instance of GitHub info class
 * as field in the Addon class.
 */

@Repository
public interface GitHubInfoRepository extends JpaRepository<GitHubInfo, Integer> {
}
