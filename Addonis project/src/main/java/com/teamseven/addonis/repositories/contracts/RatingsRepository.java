package com.teamseven.addonis.repositories.contracts;

import com.teamseven.addonis.models.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
/**
 * This is a class that provides functionality connected with the Rating class.
 */
@Repository
public interface RatingsRepository extends JpaRepository<Rating,Integer> {

    void findByRatingId(int id);

    boolean existsByUser_IdAndAddon_AddonId(int userId, int addonId);
    Rating findByUser_IdAndAddon_AddonId(int userId, int addonId);

    @Query("select avg(ar.ratingVal) from Rating ar where ar.addon.addonId = :#{#addonId}")
    Double findAverageRatingOfAddon(int addonId);

}
