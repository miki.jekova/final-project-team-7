package com.teamseven.addonis.repositories.contracts;

import com.teamseven.addonis.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * This interface serves to retrieve database records for the User class.
 */
public interface UsersRepository extends JpaRepository<User, Integer> {

    User findByIdAndEnabledIsTrue(int id);

    User findByUsernameAndEnabledIsTrue(String username);

    List<User> findAllByEnabledIsTrue();

    boolean existsByUsername(String username);

    boolean existsByIdAndEnabledIsTrue(int id);

    boolean existsByUsernameAndEnabledIsTrue(String username);

}
