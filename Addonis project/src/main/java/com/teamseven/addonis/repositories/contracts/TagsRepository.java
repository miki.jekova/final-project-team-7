package com.teamseven.addonis.repositories.contracts;

import com.teamseven.addonis.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * This is a class that provides functionality connected with the Tag class.
 */
@Repository
public interface TagsRepository extends JpaRepository<Tag, Integer> {

    List<Tag> findAll();

    Tag findTagByTagId(int id);

    Tag findTagByTag(String tag);

    boolean existsByTag(String name);

    boolean existsByTagId(int id);
}
