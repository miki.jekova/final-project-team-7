package com.teamseven.addonis.repositories.contracts;

import com.teamseven.addonis.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * This is a class that provides functionality connected with the Status class.
 */
@Repository
public interface StatusRepository extends JpaRepository<Status,Integer> {

    Status findByStatus(String status);
}
