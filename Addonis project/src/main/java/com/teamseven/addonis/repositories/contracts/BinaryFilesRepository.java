package com.teamseven.addonis.repositories.contracts;

import com.teamseven.addonis.models.BinaryFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * This is a class that is used for saving the Binary file as field in the Addon class.
 */

@Repository
public interface BinaryFilesRepository extends JpaRepository<BinaryFile, Integer> {
}
